﻿using UnityEngine;

public class Player : MonoBehaviour
{
    private Vector3 lastPosition = Vector3.one;
    private float prevMinDistance = 0.0f;

    void Update()
    {
        if (lastPosition != transform.position)
        {
            //determine amount and direction moved
            float distanceMoved = Vector3.Distance(lastPosition, transform.position);

            ColorChangingObject closestObject = null;
            float currDistance = 0.0f;
            foreach (var item in ColorChangeObjectContainer.Instance.colorChangingObjects)
            {
                //if potential min distance from this object is less than or equal potential max distance from previous object then check
                if ((item.prevDistance - distanceMoved) <= (prevMinDistance + distanceMoved))
                {
                    float newDistance = Vector3.Distance(item.transform.position, transform.position);

                    if (closestObject == null || newDistance < currDistance)
                    {
                        if (closestObject != null)
                            closestObject.ResetColor();
                        closestObject = item;
                        currDistance = newDistance;
                        continue;
                    }
                }
                item.ResetColor();
            }

            prevMinDistance = currDistance;
            //if the object is the closest of them all change its color
            if (closestObject != null)
                closestObject.ChangeColor();
            lastPosition = transform.position;
        }
        else
            ColorChangeObjectContainer.Instance.CheckIfColorUpdateRequired();
    }

    public void DemondUpdate()
    {
        ColorChangingObject closestObject = null;
        float currDistance = 0.0f;
        foreach (var item in ColorChangeObjectContainer.Instance.colorChangingObjects)
        {
            float newDistance = Vector3.Distance(item.transform.position, transform.position);

            if (closestObject == null || newDistance < currDistance)
            {
                if (closestObject != null)
                    closestObject.ResetColor();
                closestObject = item;
                currDistance = newDistance;
            }
            else
                item.ResetColor();
        }

        prevMinDistance = currDistance;
        //if the object is the closest of them all change its color
        if (closestObject != null)
            closestObject.ChangeColor();
        lastPosition = transform.position;
    }
}
