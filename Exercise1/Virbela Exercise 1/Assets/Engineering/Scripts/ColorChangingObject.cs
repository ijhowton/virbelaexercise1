﻿///Base class that will be used to handle changing the color of the object
///I decided to handle this via a base class since it will be shared functionality and all that would need to be set up would be the desired changed color

using UnityEngine;

//Requiring the sprite renderer component means I don't need to null check in the awake function since the game will not compile without the script being there
[RequireComponent(typeof(SpriteRenderer))]
public class ColorChangingObject : MonoBehaviour
{
    //A simple boolean to know which color should be used if a full color reset is required.
    //This is only really used when the color is changed in the editor and all the objects need to update their colors. This way no further calucations need to be done to update the displayed colors
    protected bool currentlyBaseColor = true;
    //This previous distance this object was from the player object. This will be used to determine if full distance calculations are required each update
    public float prevDistance = 0.0f;
    //This sprite renderer attached to this object will be needed to adjust the color
    protected SpriteRenderer thisRenderer;

    //On Start the object will grab a reference to the sprite renderer that should be on the object and hold onto it for internal use
    public void Start()
    {
        thisRenderer = GetComponent<SpriteRenderer>();
    }

    //This function will simply reset the current color to the base color
    //The base class version just null checks the sprite renderer and updates the currentlyBaseColor flag since setting up the correct color is done in the child class
    public virtual void ResetColor()
    {
        if(thisRenderer == null)
            thisRenderer = GetComponent<SpriteRenderer>();
        currentlyBaseColor = true;
    }

    //This function will set the current color to the desired changed color
    //The base class version just null checks the sprite renderer and updates the currentlyBaseColor flag since setting up the correct color is done in the child class
    public virtual void ChangeColor()
    {
        if (thisRenderer == null)
            thisRenderer = GetComponent<SpriteRenderer>();
        currentlyBaseColor = false;
    }

    //This function will be used to update the currently displayed color based on the currentlyBaseColor flag
    //The base class version just null checks the sprite renderer and updates the currentlyBaseColor flag since setting up the correct color is done in the child class
    public virtual void UpdateDisplayedColor()
    {
        if (thisRenderer == null)
            thisRenderer = GetComponent<SpriteRenderer>();
    }
}
