﻿///The Bot class is a child of the ColorChangingObject class and uses the inherited functions to correctly set the colors for the Bot objects.

using UnityEngine;

public class Bot : ColorChangingObject
{
    //This function just calls the base function and then sets the correct changed color for the bots based off the ColorChangeObjectContainer since that is acting as a shared information hub
    public override void ChangeColor()
    {
        base.ChangeColor();
        thisRenderer.color = ColorChangeObjectContainer.Instance.BotChangeColor;
    }

    //This function just calls the base function and then sets the correct reset color for the bots based off the ColorChangeObjectContainer since that is acting as a shared information hub
    public override void ResetColor()
    {
        base.ResetColor();
        thisRenderer.color = ColorChangeObjectContainer.Instance.BotBaseColor;
    }

    //This function just calls the base function and then sets the correct color for the bots based off the currentlyBaseColor flag
    //The ColorChangeObjectContainer is acting as a shared information hub for the color specifics
    public override void UpdateDisplayedColor()
    {
        base.UpdateDisplayedColor();
        if(currentlyBaseColor)
            thisRenderer.color = ColorChangeObjectContainer.Instance.BotBaseColor;
        else
            thisRenderer.color = ColorChangeObjectContainer.Instance.BotChangeColor;
    }
}