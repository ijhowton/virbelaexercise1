﻿using System.Collections.Generic;
using UnityEngine;

public class ColorChangeObjectContainer : MonoBehaviour
{
    private static ColorChangeObjectContainer instance = null;

    public static ColorChangeObjectContainer Instance
    {
        get
        {
            return instance;
        }
    }

    [SerializeField]
    private Player playerObject;

    [SerializeField]
    private Vector3 newObjectTopLeft;
    [SerializeField]
    private Vector3 newObjectBottomRight;

    [SerializeField]
    private GameObject ItemParent;
    [SerializeField]
    private GameObject ItemTemplate;

    private Color prevItemBaseColor = Color.white;
    public Color ItemBaseColor = Color.white;
    private Color prevItemChangeColor = Color.red;
    public Color ItemChangeColor = Color.red;

    [SerializeField]
    private GameObject BotParent;
    [SerializeField]
    private GameObject BotTemplate;

    private Color prevBotBaseColor = Color.white;
    public Color BotBaseColor = Color.white;
    private Color prevBotChangeColor = Color.blue;
    public Color BotChangeColor = Color.blue;

    public List<ColorChangingObject> colorChangingObjects;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }

        instance = this;
    }

    void Update()
    {
        bool demandUpdate = false;
        if(Input.GetKeyUp(KeyCode.I))
        {
            Item duplicateItem = Instantiate(ItemTemplate, GetNewRandomPosition(), Quaternion.identity, ItemParent.transform).GetComponent<Item>();
            colorChangingObjects.Add(duplicateItem);
            demandUpdate = true;
        }
        if (Input.GetKeyUp(KeyCode.B))
        {
            Bot duplicateBot = Instantiate(BotTemplate, GetNewRandomPosition(), Quaternion.identity, BotParent.transform).GetComponent<Bot>();
            colorChangingObjects.Add(duplicateBot);
            demandUpdate = true;
        }
        if (demandUpdate)
            playerObject.DemondUpdate();
    }

    public void CheckIfColorUpdateRequired()
    {
        bool updateColors = false;

        if (ItemBaseColor != prevItemBaseColor)
        {
            prevItemBaseColor = ItemBaseColor;
            updateColors = true;
        }

        if (ItemChangeColor != prevItemChangeColor)
        {
            prevItemChangeColor = ItemChangeColor;
            updateColors = true;
        }

        if (BotBaseColor != prevBotBaseColor)
        {
            prevBotBaseColor = BotBaseColor;
            updateColors = true;
        }

        if (BotChangeColor != prevBotChangeColor)
        {
            prevBotChangeColor = BotChangeColor;
            updateColors = true;
        }

        if(updateColors)
        {
            foreach (var item in colorChangingObjects)
            {
                item.UpdateDisplayedColor();
            }
        }
    }

    public Vector3 GetNewRandomPosition()
    {
        Vector3 newPosition = Vector3.zero;
        newPosition.x = Random.Range(newObjectTopLeft.x, newObjectBottomRight.x);
        newPosition.y = Random.Range(newObjectBottomRight.y, newObjectTopLeft.y);

        return newPosition;
    }
}
